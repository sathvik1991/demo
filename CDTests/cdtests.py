"""CD Automation tests."""
import requests
import unittest
from faker import Faker


host = "http://127.0.0.1:8000"
url1 = host + "/phonebook/v1/create/contact/"
url2 = host + "/phonebook/v1/update/contact/"
url3 = host + "/phonebook/v1/delete/contact/"
myFactory = Faker()
newcontact1 = {
    "user_name": 1,
    "name": myFactory.name(),
    "mobile_number": "8142282815",
    "Home_number": ""
}

newcontact2 = {
    "user_name": 1,
    "name": myFactory.name(),
    "mobile_number": "8142282816",
    "Home_number": ""
}


class CDTests(unittest.TestCase):
    """Running CD Tests."""

    def test_add_new_contact(self):
        """Testing Adding New Contact API."""
        p = requests.post(url1, data=newcontact1)
        q = requests.post(url1, data=newcontact2)
        self.assertEqual(201, p.status_code)
        self.assertEqual(201, q.status_code)

    def test_get_contacts(self):
        """Testing Listing all Contacts API."""
        r = requests.get(url1)
        self.assertEqual(200, r.status_code)

    def test_update_contact(self):
        """Testing Update Contact API."""
        r = requests.get(url1)
        totallen = len(r.json()['contactlist'])
        pk = r.json()['contactlist'][totallen - 1]['pk']
        update = requests.get(url2 + str(pk))
        update = update.json()
        # print update
        updatecontact = {
            "user_name": update['user_name'],
            "name": update['name'],
            "mobile_number": update['mobile_number'],
            "Home_number": update['mobile_number']
        }
        p = requests.post(url2 + str(pk), data=updatecontact)
        """
        print p.status_code
        update1 = requests.get(url2 + str(pk))
        update1 = update1.json()
        print update1
        """
        self.assertEqual(200, p.status_code)

    def test_delete_contact(self):
        """Testing Deleting a Contact."""
        r = requests.get(url1)
        totallen = len(r.json()['contactlist'])
        print totallen
        pk = r.json()['contactlist'][totallen - 1]['pk']
        delete = requests.delete(url3 + str(pk))
        """
        print delete.status_code
        r = requests.get(url1)
        totallen2 = len(r.json()['contactlist'])
        print totallen2
        """
        self.assertEqual(204, delete.status_code)


if __name__ == '__main__':
    unittest.main()
