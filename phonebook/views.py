"""Views for Phone Book App."""
# Create your views here.
from django.http import HttpResponse
from phonebook.serializers import (
    ContactsListSerializer,
    ContactsCreateSerializer,
)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from phonebook.models import Contacts
from django.http import Http404
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
)
from django.shortcuts import (
    get_object_or_404
)


def index(request):
    """."""
    return HttpResponse("Hello, world. You're at the polls index.")


class ContactsListView(ListCreateAPIView):
    """List all Contacts, or create a new Contacts."""

    serializer_class = ContactsCreateSerializer
    # permission_classes = (IsAuthenticated,)
    # renderer_classes = (TemplateHTMLRenderer,)
    # template_name = 'createcluster.html'

    def get(self, request, format=None):
        """Get Method."""
        contactlist = Contacts.objects.all()
        serializer = ContactsListSerializer(contactlist, many=True)
        contactlist = {}
        contactlist['contactlist'] = serializer.data
        return Response(contactlist)

    def post(self, request, format=None):
        """Post Method."""
        serializer = ContactsCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ContactsDetailView(RetrieveAPIView):
    """
    This is a API for the Contacts Model to fetch the details of vc entries.

    Lookup field is pk
    Method: POST
    """

    # permission_classes = [AllowAny]
    queryset = Contacts.objects.all()
    serializer_class = ContactsListSerializer
    lookup_field = 'pk'


class ContactsUpdateView(RetrieveUpdateAPIView):
    """
    This is a API for the Contacts Model to update the details of vc entries.

    Lookup field is pk
    Method: GET, POST
    """

    # authentication_classes = ([TokenAuthentication, SessionAuthentication])
    # permission_classes = [IsAuthenticated]
    # renderer_classes = ([TemplateHTMLRenderer, JSONRenderer])
    # template_name = 'vc/updatevc.html'
    lookup_field = 'pk'
    queryset = Contacts.objects.all()
    serializer_class = ContactsCreateSerializer

    def get_object(self, pk):
        """Get Object Method."""
        try:
            # print pk
            return Contacts.objects.get(pk=pk)
        except Contacts.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """Get Method."""
        contact = self.get_object(pk)
        contact = ContactsCreateSerializer(contact)
        return Response(contact.data)

    def post(self, request, pk, format=None, *args, **kwargs):
        """POST Method."""
        contact = get_object_or_404(Contacts, pk=pk)
        # print vc
        serializer = ContactsCreateSerializer(contact, data=request.data)
        if serializer.is_valid():
            # print "IS valid"
            serializer.save()
            return Response(serializer.data)


class ContactsDeleteView(DestroyAPIView):
    """
    This is a API for the Contacts Model to delete a vc entries.

    Lookup field is pk
    Method: POST
    """

    queryset = Contacts.objects.all()
    serializer_class = ContactsListSerializer
    lookup_field = 'pk'


class ContactsListDetailView(APIView):
    """Retrieve, update or delete a Cluster instance."""

    def get_object(self, pk):
        """Get Object Method."""
        try:
            # print pk
            return Contacts.objects.get(pk=pk)
        except Contacts.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """Get Method."""
        contact = self.get_object(pk)
        contact = ContactsListSerializer(contact)
        return Response(contact.data)

    def put(self, request, pk, format=None):
        """PUT Method."""
        contact = self.get_object(pk)
        serializer = ContactsListSerializer(contact, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """Delete Method."""
        contact = self.get_object(pk)
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserContactView(APIView):
    """
    GET the list of clusters assigned to the given user.

    To GET the list of clusters not assigned to any user. pass pk=0 in the url.
    """

    serializer_class = ContactsListSerializer
    # renderer_classes = [TemplateHTMLRenderer, ]
    # template_name = 'cluster/listcluster.html'
    # permission_classes = [AllowAny]

    def get_object(self, pk):
        """GET Object for given PK."""
        try:
            if int(pk) != 0:
                return Contacts.objects.filter(user_name=pk)
            else:
                return Contacts.objects.filter(user_name=True)
        except Contacts.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """GET Method."""
        # print request.user.pk
        contact = self.get_object(pk)
        contact = ContactsListSerializer(contact, many=True)
        return Response(contact.data,)
