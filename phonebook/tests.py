"""Test Cases."""
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from phonebook.models import Contacts
# Create your tests here.


class ContactTestCase(TestCase):
    """UnitTests for Contacts Model."""

    def setUp(self):
        """Seting UP DB."""
        self.user = User.objects.create_user(
            username='jacob',
            email=' ',
            password='top_secret'
        )
        Contacts.objects.create(
            user_name=self.user,
            name="Sathvik",
            mobile_number="+918142282819")

    def test_retrive_mobile_number(self):
        """Retriving mobile number based on username."""
        contact = Contacts.objects.get(name="Sathvik")
        self.assertEqual(contact.mobile_number, "+918142282819")

    def test_modile_number_validation(self):
        """Test Case for validating mobile number format."""
        contact = Contacts(
            user_name=self.user,
            name="Msathvik",
            mobile_number="7777777")
        with self.assertRaises(ValidationError):
            # `full_clean` will raise a ValidationError
            #   if any fields fail validation
            if contact.full_clean():
                contact.save()
        self.assertEqual(Contacts.objects.filter(name='Msathvik').count(), 0)

    def test_count_contact_numbers(self):
        """Verifying the count of the contacts for a user."""
        all_contacts = Contacts.objects.all().count()
        self.assertEqual(all_contacts, 1)

    def test_home_number(self):
        """Verifying the count of the contacts for a user."""
        contact = Contacts.objects.get(name="Sathvik")
        self.assertEqual(contact.Home_number, "")
