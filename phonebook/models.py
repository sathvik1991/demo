from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator


# Create your models here.
class Contacts(models.Model):
    user_name = models.ForeignKey(
        User,
        null=True,
        help_text='User Name'
    )
    name = models.CharField(
        max_length=100,
        verbose_name="Name of the person",
    )
    phone_regex = RegexValidator(
        # regex=r'^(\+9[\-\s]?)?[0]?(91)?[789]\d{13}$',
        # regex=r'^(?:\+?91)?[07]\d{9,13}$',
        regex=r'(?:\s+|)((0|(?:(\+|)91))(?:\s|-)*(?:(?:\d(?:\s|-)*\d{9})|(?:\d{2}(?:\s|-)*\d{8})|(?:\d{3}(?:\s|-)*\d{7}))|\d{10})(?:\s+|)',
        message="Please Enter a Valied Phone Number."
    )
    mobile_number = models.CharField(
        validators=[phone_regex],
        max_length=13,
        blank=False)
    # validators should be a list
    Home_number = models.CharField(max_length=12, blank=True)
    # work_number = models.CharField(max_length=12, blank=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        """."""

        db_table = "Contacts"
        ordering = ["timestamp", "updated"]
