"""Urls for Phone Book App."""
from django.conf.urls import url

from phonebook.views import (
    index,
    ContactsListView,
    ContactsListDetailView,
    UserContactView,
    ContactsUpdateView,
    ContactsDeleteView,
    ContactsDetailView,
)


urlpatterns = [
    url(r'^$', index, name='index'),
    # All Cluster related CURD operation urls are below.

    url(r'^v1/create/contact/$', ContactsListView.as_view(),
        name='createcontact'
        ),
    url(r'^v1/contact/(?P<pk>[0-9]+)/$', ContactsListDetailView.as_view()),
    url(r'^v1/detail/contact/(?P<pk>[0-9]+)$',
        ContactsDetailView.as_view(), name='listcontact'
        ),
    url(r'^v1/update/contact/(?P<pk>[0-9]+)$',
        ContactsUpdateView.as_view(), name='updatecontact'
        ),
    url(r'^v1/delete/contact/(?P<pk>[0-9]+)$',
        ContactsDeleteView.as_view(),
        name='deletecontact'
        ),
    url(r'^v1/usercontact/(?P<pk>[0-9]+)/$', UserContactView.as_view()),
]
