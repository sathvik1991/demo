"""."""
from rest_framework.serializers import ModelSerializer
from .models import Contacts
from rest_framework import serializers


class ContactsListSerializer(ModelSerializer):
    """Cluster List Serializer."""

    '''
    Displaying foreign Key value logic(CharField) is implemented based on
    example in below link
    http://stackoverflow.com/questions/28200485/foreign-key-value-in-django-rest-framework
    '''
    username = serializers.CharField(source='user_name', read_only=True)

    class Meta:
        """Meta Class."""

        model = Contacts
        fields = [
            "pk",
            "username",
            "name",
            "mobile_number",
            "Home_number",
            "updated",
        ]


class ContactsCreateSerializer(ModelSerializer):
    """Cluster List Serializer."""

    '''
    Displaying foreign Key value logic(CharField) is implemented based on
    example in below link
    http://stackoverflow.com/questions/28200485/foreign-key-value-in-django-rest-framework
    '''
    # username = serializers.CharField(source='user_name', read_only=True)

    class Meta:
        """Meta Class."""

        model = Contacts
        fields = [
            "user_name",
            "name",
            "mobile_number",
            "Home_number",
        ]