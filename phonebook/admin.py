from django.contrib import admin
from .models import Contacts
from django.forms import ModelForm
# Register your models here.


class ContactsForm(ModelForm):
    """."""

    class Meta:
        """."""

        model = Contacts
        fields = [
            "user_name",
            "name",
            "mobile_number",
            "Home_number",
        ]


class ContactsListModelAdmin(admin.ModelAdmin):
    """."""

    list_display = [
        "user_name",
        "name",
        "mobile_number",
        "Home_number",
        "updated",
    ]

    class Meta:
        """."""

        model = Contacts


admin.site.register(Contacts, ContactsListModelAdmin)
