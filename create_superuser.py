#!/usr/bin/env python
"""."""
# from django.core.management import setup_environ
# import settings
import os
import django
# from django.conf import settings
# from demosite import settings as setting


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "demosite.settings")
django.setup()

from django.contrib.auth.models import User


User.objects.create_superuser('admin', 'admin@example.com', 'Vedams@123')
