# README #

This is a Django Demo Project. Developed to give a Demo on Django Basics. 

### What is this repository for? ###

* Online Phone Book is the main goal of this project.

### How do I get set up? ###

* Install virtualenv.
* Activate virtualenv.
* Install Dependencies from requirements file.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Sathvik M.
* @ sathvik.1991@gmail.com.